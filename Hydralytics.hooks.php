<?php
/**
 * Curse Inc.
 * Hydralytics
 * Hydralytics Hooks
 *
 * @author		Brent Copeland
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Hydralytics
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class HydralyticsHooks {
	/**
	 * Setup extra configuration during MediaWiki setup process.
	 *
	 * @return boolean	True
	 */
	public static function onRegistration() {
		global $wgSyncServices;

		$wgSyncServices["DailyAnalyticsJob"] = "Hydralytics\\DailyAnalyticsJob";
		$wgSyncServices["SiteStatsDailyJob"] = "Hydralytics\\SiteStatsDailyJob";
		$wgSyncServices["SiteStatsHourlyJob"] = "Hydralytics\\SiteStatsHourlyJob";

		return true;
	}

	/**
	 * Setups and Modifies Database Information
	 *
	 * @access	public
	 * @param	object	[Optional] DatabaseUpdater Object
	 * @return	boolean true
	 */
	static public function onLoadExtensionSchemaUpdates(DatabaseUpdater $updater) {
		$extDir = __DIR__;

		$updater->addExtensionUpdate(['addTable', 'analytics_daily', "{$extDir}/install/sql/hydralytics_table_analytics_daily.sql", true]);
		$updater->addExtensionUpdate(['addTable', 'analytics_hourly_edits', "{$extDir}/install/sql/hydralytics_table_analytics_hourly_edits.sql", true]);

		//2014-04-01
		$updater->addExtensionUpdate(['addIndex', 'analytics_daily', 'date', "{$extDir}/upgrade/sql/hydralytics_upgrade_analytics_daily_add_date_index.sql", true]);
		$updater->addExtensionUpdate(['addIndex', 'analytics_hourly_edits', 'date', "{$extDir}/upgrade/sql/hydralytics_upgrade_analytics_hourly_edits_add_date_index.sql", true]);

		return true;
	}
}
