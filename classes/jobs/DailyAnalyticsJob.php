<?php
/**
 * Curse Inc.
 * Hydralytics
 * Daily Analytics Refresh Job
 *
 * @author		Alexia E. Smith, Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Hydralytics
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace Hydralytics;

use RedisCache;

class DailyAnalyticsJob extends \SyncService\Job {
	/**
	 * Update Google Analytic statistics in Redis.
	 *
	 * @access	public
	 * @param	array	[Unused] Job arguments.
	 * @return	integer Exit value for this process.
	 */
	public function execute($args = []) {
		$siteKeys = [];

		$redis = RedisCache::getClient('worker');

		//@TODO: Likely broken when forking.
		$db = wfGetDB(DB_SLAVE);
		$results = $db->select(
			[
				'wiki_sites',
				'wiki_advertisements'
			],
			[
				'wiki_sites.md5_key',
				'wiki_advertisements.googleanalyticsid'
			],
			[
				'wiki_sites.deleted'	=> 0,
				"googleanalyticsid NOT LIKE ''"
			],
			__METHOD__,
			[],
			[
				'wiki_advertisements' => [
					'INNER JOIN', 'wiki_advertisements.site_key = wiki_sites.md5_key'
				]
			]
		);

		while ($row = $results->fetchRow()) {
			$siteKeys[$row['md5_key']] = Information::extractGAProperty($row['googleanalyticsid']);
		}
		$results->free();

		$ga = new GoogleAnalytics();

		$ga->loadGAInfo($siteKeys);

		try {
			$redis->del('hydralytics:gaDailyData');

			foreach ($siteKeys as $siteKey => $property) {
				// Grab the 30/60/90 data
				$gaData = $ga->getTotalsData(strtotime('31 days ago'), strtotime('1 day ago'), $siteKey);
				if ($gaData['success'] !== true) {
					$this->outputLine('Failed to get 30 day totals for last month for '.$siteKey.' - '.$property.'.	 Error was: '.$gaData['error']);
					continue;
				}

				$gaDataOld = $ga->getTotalsData(strtotime('61 days ago'), strtotime('31 days ago'), $siteKey);
				if ($gaDataOld['success'] !== true) {
					$this->outputLine('Failed to get 30 day totals for two months ago for '.$siteKey.' - '.$property.'.	 Error was: '.$gaDataOld['error']);
					continue;
				}

				$gaDailyData = $ga->getDailyData(strtotime('91 days ago'), strtotime('1 days ago'), $siteKey);
				if ($gaDailyData['success'] !== true) {
					$this->outputLine('Failed to get daily totals for the past 90 days for '.$siteKey.' - '.$property.'.  Error was: '.$gaDailyData['error']);
					continue;
				}

				foreach ($gaData['data'] as $key => $val) {
					$gaData['data'][$key.'_delta'] = $val - $gaDataOld['data'][$key];
				}

				$trend = 0;
				$dailyThirtyDelta = 0;
				if ($gaDailyData['success'] === true) {
					// compare today's daily with daily from 30 days ago
					$thirtyDayAgoKey = array_keys($gaDailyData['data']['ga:users'])[count($gaDailyData['data']['ga:users'])-31];
					if (isset($gaDailyData['data'][$thirtyDayAgoKey])) {
						$dailyThirtyDelta = end($gaDailyData['data']['ga:users']) - $gaDailyData['data'][$thirtyDayAgoKey];
					}

					// count length of visitors trend over the past 90 days
					$trendLength = 0;
					$prevTrendDirection = 0;
					// iterate over $gaDailyData['data'] in reverse
					foreach (array_reverse(array_keys($gaDailyData['data']['ga:users_delta'])) as $day) {
						$trendDirection = $this->signum($gaDailyData['data']['ga:users_delta'][$day]);
						// if we haven't yet established a direction, use the current one
						if ($prevTrendDirection == 0) {
							$prevTrendDirection = $trendDirection;
						}
						// if we are holding steady or continuing in the same direciton, keep going
						if ($trendDirection == 0 || $trendDirection == $prevTrendDirection) {
							$trendLength++;
						} else {
							// found the point where the trend changes direction
							$trend = $trendLength * $prevTrendDirection;
							break;
						}
					}

					// TODO if $trend is 90 indicate that it is maxed out?
					$gaData['ga:dailyTrend'] = $trend;
					$gaData['ga:dailyThirtyDelta'] = $dailyThirtyDelta;
				} else {
					$this->outputLine('Failed to get daily data for '.$siteKey.' - '.$property.'.  Error was: '.$gaDailyData['error']);
					continue;
				}

				$this->outputLine('Entering daily data for: '.$siteKey.' - '.$property);
				$redis->hSet('hydralytics:gaDailyData', $siteKey, serialize($gaData['data']));
			}

			$redis->set('hydralytics:gaDailyDataUpdatedAt', time());
		} catch (\RedisException $e) {
			throw new \MWException(__METHOD__.": Caught RedisException - ".$e->getMessage());
		}
	}

	/**
	 * Clamp negatives to -1 and positives to +1
	 *
	 * @access	private
	 * @param	integer Number
	 * @return	integer Clamped Number
	 */
	private function signum($number) {
		return min(1, max(-1, intval($number)));
	}

	/**
	 * Return cron schedule if applicable.
	 * 4AM Central every day.
	 *
	 * @access	public
	 * @return	mixed	False for no schedule or an array of schedule information.
	 */
	static public function getSchedule() {
		return [
			[
				'minutes' => '0',
				'hours' => '9',
				'days' => '*',
				'months' => '*',
				'weekdays' => '*',
				'arguments' => []
			]
		];
	}
}