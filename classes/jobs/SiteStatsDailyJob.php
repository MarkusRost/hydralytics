<?php
/**
 * Curse Inc.
 * Hydralytics
 * Site Stats Daily Job
 *
 * @author		Alex Smith, Brent Copeland
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Hydralytics
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace Hydralytics;

class SiteStatsDailyJob extends \SyncService\Job {
	/**
	 * Job to update the daily analytics table.
	 *
	 * @access	public
	 * @param	array	[Unused] Job arguments.
	 * @return	integer Exit value for this process.
	 */
	public function execute($args = []) {
		//@TODO: Likely broken when forking.
		$db = wfGetDB(DB_MASTER);

		$values = [];
		//get stats

		$this->outputLine("Gathering Daily Site Statistics\n\n");
		$counter = new \SiteStatsInit();

		$this->outputLine("Counting total edits...");
		$edits = $counter->edits();
		$this->outputLine("{$edits}\nCounting number of articles...");

		$articles  = $counter->articles();
		$this->outputLine("{$articles}\nCounting total pages...");

		$pages = $counter->pages();
		$this->outputLine("{$pages}\nCounting number of users...");

		$users = $counter->users();
		$this->outputLine("{$users}\nCounting number of images...");

		$images = $counter->files();
		$this->outputLine("{$images}\n");

		$counter->refresh();

		$this->outputLine("Counting active users...");
		$activeUsers = \SiteStatsUpdate::cacheUpdate(wfGetDB(DB_MASTER));
		$this->outputLine("{$activeUsers}\n");

		$this->outputLine("Date added...");
		$date = time();
		$this->outputLine("{$date}\n");

		$result = $db->select(
			['analytics_daily'],
			['*'],
			['adid > 0'],
			__METHOD__,
			[
				'ORDER BY' => 'adid DESC',
				'LIMIT' => '1'
					]
		);

		if ($result) {
			$this->outputLine("Found Deltas\n");
			foreach ($result as $row) {
				if (date('z', $row->date) == date('z', $date)) {
					$this->outputLine("Stats already gathered for today.\n");
					return false;
				}
				$editsDelta			= intval($edits - $row->edits);
				$articlesDelta		= intval($articles - $row->good_articles);
				$pagesDelta			= intval($pages - $row->pages);
				$usersDelta			= intval($users - $row->users);
				$imagesDelta		= intval($images - $row->images);
				$viewsDelta			= intval($views - $row->views);
				$activeUsersDelta	= intval($activeUsers - $row->active_users);
			}
		} else {
			$this->outputLine("No Previous Deltas - initial stats run probably\n");
			$editsDelta			= intval($edits);
			$articlesDelta		= intval($articles);
			$pagesDelta			= intval($pages);
			$usersDelta			= intval($users);
			$imagesDelta		= intval($images);
			$viewsDelta			= intval($views);
			$activeUsersDelta	= intval($activeUsers);
		}

		$this->outputLine("\nUpdating site statistics...");

		$values = [
			'views'					=> $views,
			'views_delta'			=> $viewsDelta,
			'edits'					=> $edits,
			'edits_delta'			=> $editsDelta,
			'good_articles'			=> $articles,
			'good_articles_delta'	=> $articlesDelta,
			'pages'					=> $pages,
			'pages_delta'			=> $pagesDelta,
			'users'					=> $users,
			'users_delta'			=> $usersDelta,
			'active_users'			=> $activeUsers,
			'active_users_delta'	=> $activeUsersDelta,
			'images'				=> $images,
			'images_delta'			=> $imagesDelta,
			'date'					=> $date,
		];

		foreach ($values as $title => $data){
			$this->outputLine("Title: {$title} Data: {$data} .\n");
		}

		$this->outputLine("Updating values...");

		//put stats
		if($db->insert('analytics_daily', $values, __METHOD__)){
			$this->outputLine("adding daily stats complete.\n");
			return 0;
		} else {
			$this->outputLine("Could not add daily stats.\n");
			return 1;
		}
	}

	/**
	 * Return cron schedule if applicable.
	 * 5AM Central every day.
	 *
	 * @access	public
	 * @return	mixed	False for no schedule or an array of schedule information.
	 */
	static public function getSchedule() {
		return [
			[
				'minutes' => '0',
				'hours' => '10',
				'days' => '*',
				'months' => '*',
				'weekdays' => '*',
				'arguments' => []
			]
		];
	}
}
